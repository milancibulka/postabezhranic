import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
   providedIn: 'root'
})
export class ApiService {

   constructor(private http: HttpClient) { }

   public sendData(data: string, token: string): Observable<Object> {
      let headers = new HttpHeaders();
      headers = headers.set('Content-Type', 'text/plain').set('Authorization', 'Basic ' + token);
      let httpOptions = {
         headers: headers
      };
      return this.http.post('https://cors-anywhere.herokuapp.com/http://dev.postabezhranic.cz/administrace/api2/', { "cinnost": "nicePlacesReceipt", "id": "1234567", "kod": data }, httpOptions);
   }

   public authorizeLogin(token: string): Observable<Object> {
      let headers = new HttpHeaders();
      headers = headers.set('Content-Type', 'text/plain').set('Authorization', 'Basic ' + token);
      let httpOptions = {
         headers: headers
      };
      return this.http.post('https://cors-anywhere.herokuapp.com/http://dev.postabezhranic.cz/administrace/api2/', { "cinnost": "authorizeLogin" }, httpOptions);
   }
}

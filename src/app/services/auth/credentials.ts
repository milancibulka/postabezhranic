export interface Credentials {
   name: string;
   token: string;
}

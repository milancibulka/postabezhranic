import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { ApiService } from 'src/app/api/api.service';
import { BehaviorSubject, Subscription, Observable, from } from 'rxjs';
import * as CryptoJS from 'crypto-js';
import { Credentials } from './credentials';

@Injectable({
   providedIn: 'root'
})
export class AuthService {
   private newCredentials: Credentials = { name: null, token: null };
   private credentials: BehaviorSubject<Credentials> = new BehaviorSubject<Credentials>(this.newCredentials);
   private subscription: Subscription;

   constructor(private storage: NativeStorage, private apiService: ApiService) { }

   public getCredentials(): Observable<Credentials> {
      this.unsubscribeData();
      this.subscription = from(this.loadCredentials()).subscribe((data: Credentials) => {
         this.credentials.next(data);
      });
      return this.credentials.asObservable();
   }

   private unsubscribeData(): void {
      if (this.subscription) {
         this.subscription.unsubscribe();
      }
   }

   public login(name: string, password: string): Promise<string> {
      return new Promise((resolve) => {
         let token = this.createToken(name, password);
         this.apiService.authorizeLogin(token).toPromise().then(
            (data) => {
               if (data) {
                  let response: any = {};
                  response = data;
                  if (response.stav == 'ok') {
                     this.saveCredentials(name, token)
                  }
                  resolve(response.stav_info);
               }
            },
            (error) => {
               resolve(error.error.stav_info);
            }
         );
      });
   }

   private createToken(name: string, password: string): string {
      let hash = CryptoJS.SHA256(password).toString();
      return btoa(name + ':' + hash);
   }

   private saveCredentials(name: string, token: string): void {
      this.storage.setItem('loggedUser', name);
      this.storage.setItem('token', token);
   }

   private loadCredentials(): Promise<Credentials> {
      return new Promise((resolve) => {
         this.storage.getItem('loggedUser').then(
            (loggedUser) => {
               this.storage.getItem('token').then(
                  (token) => {
                     resolve({name: loggedUser, token: token});
                  },
                  (error) => {
                     resolve({name: null, token: null});
                  }
               )
            },
            (error) => {
               resolve({name: null, token: null});
            },  
         );
         
      });
   }

   public logout(): void {
      this.storage.remove('token');
      this.storage.remove('loggedUser');
   }
}

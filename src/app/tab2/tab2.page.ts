import { Component } from '@angular/core';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';
import { ApiService } from '../api/api.service';
import { AuthService } from '../services/auth/auth.service';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { Subscription } from 'rxjs';
import { Credentials } from '../services/auth/credentials';
import { Router } from '@angular/router';

@Component({
   selector: 'app-tab2',
   templateUrl: 'tab2.page.html',
   styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
   private options: BarcodeScannerOptions;
   private scannedData: any = {};
   private responseData: any = {};
   private token: string = null;
   private subscription: Subscription;

   constructor(public scanner: BarcodeScanner,
      private apiService: ApiService,
      private authService: AuthService,
      private spinnerDialog: SpinnerDialog,
      private router: Router
   ) {
   }

   ionViewWillEnter() {
      if (this.subscription) {
         this.subscription.unsubscribe();
      }
      this.subscribeToken();
      if (this.token == null) {
         this.router.navigate(['/tabs/tab1']);
      }
   }

   ionViewWillLeave() {
      this.subscription.unsubscribe();
   }

   public subscribeToken(): void {
      this.subscription = this.authService.getCredentials().subscribe((data: Credentials) => {
         this.token = data.token;
      });
   }

   public scan(): void {
      this.options = {
         prompt: "Scan your barcode",
         showTorchButton: true,
         resultDisplayDuration: 0
      };
      this.scanner.scan(this.options).then((data) => {
         if (data.text) {
            this.scannedData = data;
            if (this.token != null) {
               this.spinnerDialog.show('Please wait', 'Connecting to server', true);
               this.apiService.sendData(data.text, this.token).toPromise().then((data) => {
                  this.spinnerDialog.hide();
                  this.responseData = data;
                  alert(
                     "Status: " + this.responseData.stav + "\n" +
                     "Info: " + this.responseData.stav_info
                  );
                  this.scan();
               });
            } 
         }
      },
         (error) => {
            console.log(error + ' - scan error');
         }
      );
   }
}

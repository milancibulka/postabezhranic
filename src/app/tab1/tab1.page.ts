import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../services/auth/auth.service';
import { from, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators'
import { Credentials } from '../services/auth/credentials';
import { Router, NavigationStart } from '@angular/router';


@Component({
   selector: 'app-tab1',
   templateUrl: 'tab1.page.html',
   styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
   private loggedUser: string = null;
   private subscription: Subscription;
   constructor(private authService: AuthService, private router: Router) {
      this.router.events.pipe(
         filter(event => event instanceof NavigationStart)
       ).subscribe((route: NavigationStart) => {
          if(route.url == '/tabs/tab1'){
            console.log('Route: '+route.url);
            this.subscribeLoggedUser();
          }
       });
   }

   ionViewDidLoad() {
      this.subscribeLoggedUser();
      if (this.loggedUser == null) {
         this.router.navigate(['login']);
      }
   }

   ionViewWillEnter() {
      this.subscribeLoggedUser();
   }

   public logout(): void {
      this.authService.logout();
      if (this.subscription) {
         this.subscription.unsubscribe();
      }
      this.subscribeLoggedUser();
      this.router.navigate(['login']);
   }

   public login(): void {
      this.router.navigate(['login']);
   }

   private subscribeLoggedUser(): void {
      if (this.subscription) {
         this.subscription.unsubscribe();
      }
      this.subscription = this.authService.getCredentials().subscribe((data: Credentials) => {
         this.loggedUser = data.name;
      });
   }

   ionViewWillLeave() {
      this.subscribeLoggedUser();
   }

   ionViewDidLeave() {
      this.subscription.unsubscribe();
   }
}

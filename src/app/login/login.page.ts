import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth/auth.service';
import { NgForm } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { Router } from '@angular/router';
import { NativeStorage } from '@ionic-native/native-storage/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {
   private loginMessage: string = null;

   constructor(private authService: AuthService, private spinnerDialog: SpinnerDialog, private router: Router) { }


  public authorizeLogin(form: NgForm):void
  {
    this.spinnerDialog.show('Please wait', 'Connecting to server', true);
    this.authService.login(form.value.username, form.value.password).then(
       (message) =>{
         this.loginMessage = message;
         this.spinnerDialog.hide();
         if(message == 'authorized'){
            this.router.navigateByUrl("/tabs/tab1");
         }
       }
    );
  }

}
